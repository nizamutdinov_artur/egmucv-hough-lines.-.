namespace ShapeDetection
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.secondImageBox = new Emgu.CV.UI.ImageBox();
            this.thirdImageBox = new Emgu.CV.UI.ImageBox();
            this.originalImageBox = new Emgu.CV.UI.ImageBox();
            this.loadImageButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.fileNameTextBox = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bufferButton = new System.Windows.Forms.Button();
            this.webCamBox = new System.Windows.Forms.CheckBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.secondImageBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.thirdImageBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.originalImageBox)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // secondImageBox
            // 
            this.secondImageBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.secondImageBox.Cursor = System.Windows.Forms.Cursors.Cross;
            this.secondImageBox.Location = new System.Drawing.Point(474, 59);
            this.secondImageBox.Name = "secondImageBox";
            this.secondImageBox.Size = new System.Drawing.Size(466, 249);
            this.secondImageBox.TabIndex = 4;
            this.secondImageBox.TabStop = false;
            // 
            // thirdImageBox
            // 
            this.thirdImageBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.thirdImageBox.Cursor = System.Windows.Forms.Cursors.Cross;
            this.thirdImageBox.Location = new System.Drawing.Point(12, 314);
            this.thirdImageBox.Name = "thirdImageBox";
            this.thirdImageBox.Size = new System.Drawing.Size(928, 407);
            this.thirdImageBox.TabIndex = 4;
            this.thirdImageBox.TabStop = false;
            // 
            // originalImageBox
            // 
            this.originalImageBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.originalImageBox.Cursor = System.Windows.Forms.Cursors.Cross;
            this.originalImageBox.Location = new System.Drawing.Point(12, 59);
            this.originalImageBox.Name = "originalImageBox";
            this.originalImageBox.Size = new System.Drawing.Size(456, 249);
            this.originalImageBox.TabIndex = 3;
            this.originalImageBox.TabStop = false;
            // 
            // loadImageButton
            // 
            this.loadImageButton.Location = new System.Drawing.Point(270, 12);
            this.loadImageButton.Name = "loadImageButton";
            this.loadImageButton.Size = new System.Drawing.Size(30, 23);
            this.loadImageButton.TabIndex = 0;
            this.loadImageButton.Text = "...";
            this.loadImageButton.UseVisualStyleBackColor = true;
            this.loadImageButton.Click += new System.EventHandler(this.loadImageButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "File:";
            // 
            // fileNameTextBox
            // 
            this.fileNameTextBox.Location = new System.Drawing.Point(49, 14);
            this.fileNameTextBox.Name = "fileNameTextBox";
            this.fileNameTextBox.ReadOnly = true;
            this.fileNameTextBox.Size = new System.Drawing.Size(215, 20);
            this.fileNameTextBox.TabIndex = 2;
            this.fileNameTextBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.webCamBox);
            this.panel1.Controls.Add(this.fileNameTextBox);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.bufferButton);
            this.panel1.Controls.Add(this.loadImageButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(955, 53);
            this.panel1.TabIndex = 0;
            // 
            // bufferButton
            // 
            this.bufferButton.Location = new System.Drawing.Point(306, 12);
            this.bufferButton.Name = "bufferButton";
            this.bufferButton.Size = new System.Drawing.Size(109, 23);
            this.bufferButton.TabIndex = 0;
            this.bufferButton.Text = "Load from buffer";
            this.bufferButton.UseVisualStyleBackColor = true;
            this.bufferButton.Click += new System.EventHandler(this.bufferButton_Click);
            // 
            // webCamBox
            // 
            this.webCamBox.AutoSize = true;
            this.webCamBox.Location = new System.Drawing.Point(421, 16);
            this.webCamBox.Name = "webCamBox";
            this.webCamBox.Size = new System.Drawing.Size(80, 17);
            this.webCamBox.TabIndex = 3;
            this.webCamBox.Text = "checkBox1";
            this.webCamBox.UseVisualStyleBackColor = true;
            this.webCamBox.CheckedChanged += new System.EventHandler(this.webCamBox_CheckedChanged);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 733);
            this.Controls.Add(this.thirdImageBox);
            this.Controls.Add(this.originalImageBox);
            this.Controls.Add(this.secondImageBox);
            this.Controls.Add(this.panel1);
            this.Name = "MainForm";
            this.Text = "Shape Detection";
            ((System.ComponentModel.ISupportInitialize)(this.secondImageBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.thirdImageBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.originalImageBox)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private Emgu.CV.UI.ImageBox secondImageBox;
        private Emgu.CV.UI.ImageBox thirdImageBox;
        private Emgu.CV.UI.ImageBox originalImageBox;
        private System.Windows.Forms.Button loadImageButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox fileNameTextBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button bufferButton;
        private System.Windows.Forms.CheckBox webCamBox;
        private System.Windows.Forms.Timer timer1;

    }
}

