﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Emgu.CV.Structure;
using ShapeDetection.Container;

namespace ShapeDetection.Module
{

    /// <summary>
    /// Class that support a basic shape recognition.
    /// Works only with lines.
    /// TODO: Line search here
    /// </summary>
    public class Recognition
    {

        /// <summary>
        /// Finds a roof rectangles and lines.
        /// </summary>
        /// <param name="lines">The lines.</param>
        /// <returns>The Roof.</returns>
        public static Roof[] FindRoofs(LineSegment2D[] lines)
        {
            List<Roof> roofsList = new List<Roof>();

            const int searchOffset = 10;

            for (int i = 0; i < lines.Count(); i++)
            {
                if (LineOperation.LineIncreases(lines[i]) ||
                    LineOperation.AngleWithAbscissa(lines[i]) > -110 ||
                    LineOperation.AngleWithAbscissa(lines[i]) < -170
                    )
                    continue;

                var rectangle1 = LineOperation.LineRectangle(lines[i]);

                rectangle1.Width += searchOffset;

                for (int j = 0; j < lines.Count(); j++)
                {
                    if (lines[i].Equals(lines[j]) || !LineOperation.LineIncreases(lines[j]))
                        continue;

                    var rectangle2 = LineOperation.LineRectangle(lines[j]);

                    if (!rectangle1.IntersectsWith(rectangle2) ||
                        !(lines[i].GetExteriorAngleDegree(lines[j]) > 30))
                        continue;

                    if (lines[i].P1.X >= lines[j].P1.X)
                        continue;

                    // Certer of first line X > center of second line X      
                    if (lines[i].P2.X - ((lines[i].P2.X - lines[i].P1.X)/2) >
                        lines[j].P2.X - ((lines[j].P2.X - lines[j].P1.X)/2))
                        continue;

                    if (!MathHelper.ValueInRange((int) LineOperation.AngleWithAbscissa(lines[i]), -100, -160) &&
                        !MathHelper.ValueInRange((int) LineOperation.AngleWithAbscissa(lines[j]), -20, -80))
                        continue;

                    if (lines[i].Length < lines[j].Length * 0.5 ||
                        lines[j].Length < lines[i].Length * 0.5)
                        continue;

                    if (!MathHelper.ValueInRange(
                        lines[i].P2.Y,
                        lines[j].P1.Y - searchOffset,
                        lines[j].P1.Y + searchOffset))
                        continue;

                    roofsList.Add(
                        new Roof(
                            new Rectangle(
                                rectangle1.X,
                                rectangle1.Y,
                                rectangle2.X + rectangle2.Width - rectangle1.X,
                                rectangle1.Height),
                            lines[i],
                            lines[j]
                            ));
                }
            }

            return roofsList.ToArray();
        }

        /// <summary>
        /// Finds a houses.
        /// </summary>
        /// <param name="lines">The lines.</param>
        /// <returns>The house containers.</returns>
        public static House[] FindHouses(LineSegment2D[] lines)
        {
            List<House> housesList = new List<House>();

            foreach (var roof in FindRoofs(lines))
            {
                List<Rectangle> lineRectangles = lines.Select(LineOperation.LineRectangle).ToList();

                foreach (var lineRect in lineRectangles)
                {

                    const int searchWidth = 40;

                 

                    if (roof.Rectangle.X - searchWidth >= lineRect.X ||
                        (roof.Rectangle.X + roof.Rectangle.Width + searchWidth) <= (lineRect.X + lineRect.Width) || 
                        lineRect.Y <= roof.Rectangle.Y + roof.Rectangle.Height || 
                        lineRect.Height > 5 || 
                        lineRect.Y - (roof.Rectangle.Y + roof.Rectangle.Height) <= 5) 
                        continue;

                    housesList.Add(new House(
                        new Rectangle(roof.Rectangle.X,
                            roof.Rectangle.Y + roof.Rectangle.Height,
                            roof.Rectangle.Width,
                            lineRect.Y - (roof.Rectangle.Y + roof.Rectangle.Height)),
                        roof)
                        );
                    break;
                }
            }

            return housesList.ToArray();
        }

    }
}
