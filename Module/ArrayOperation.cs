﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace ShapeDetection.Module
{
   public class ArrayOperation
    {
       public static Rectangle[] SortRectanglesByY(Rectangle[] rectangles)
       {
           List<Rectangle> rectanglesList = rectangles.ToList();

           for (int i = 0; i < rectanglesList.Count() - 1; i++)
           {
               for (int j = 0; j < rectanglesList.Count() - 1; j++)
               {
                   if (rectanglesList[j].Y > rectanglesList[j+1].Y)
                   {
                       Rectangle foo = rectanglesList[j];

                       rectanglesList[j] = rectanglesList[j + 1];
                       rectanglesList[j + 1] = foo;
                   }
               }
           }

           return new Rectangle[0];
       }

    }
}
