﻿using System.Drawing;
using Emgu.CV.Structure;

namespace ShapeDetection.Container
{
   public class House
   {
       public Rectangle WallsRectangle { get; set; }
       public Roof Roof { get; set; }

       public House(Rectangle wallsRectangle, Roof roof)
       {
           WallsRectangle = wallsRectangle;
           Roof = roof;
       }
    }
}
