﻿using System;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;
using ShapeDetection.Container;

namespace ShapeDetection.Module
{

    /// <summary>
    /// Draw.
    /// </summary>
   public class Draw
    {

       public static void Line(UMat image, Point p1, Point p2, Color color, int thick)
       {
           CvInvoke.Line(image, p1, p2, new Bgr(color).MCvScalar, 1);
       }

       public static void Rectangle(UMat image, Rectangle rectangle, Color color, int thick)
       {
           RotatedRect box = new RotatedRect(
               new PointF(rectangle.X + (float) rectangle.Width/2,
                   rectangle.Y + (float) rectangle.Height/2),
               new SizeF(rectangle.Width, rectangle.Height),
               0);

           CvInvoke.Polylines(image, Array.ConvertAll(box.GetVertices(), Point.Round), true, new Bgr(color).MCvScalar, thick);
       }

       public static void LineRectangles(UMat image, LineSegment2D[] lines)
       {
           bool countFlag = true;

           foreach (LineSegment2D line in lines)
           {
               if (countFlag)
               {
                   Draw.Rectangle(image, LineOperation.LineRectangle(line), Color.BlueViolet, 2);
               }
               else
               {
                   Draw.Rectangle(image, LineOperation.LineRectangle(line), Color.Blue, 2);
               }

               countFlag = !countFlag;
           }
       }

       public static void Lines(UMat image, LineSegment2D[] lines)
       {
           bool countFlag = true;
           Random rand = new Random();

           foreach (LineSegment2D line in lines)
           {
               if (countFlag)
               {
                   CvInvoke.Line(image, line.P1, line.P2, new Bgr(Color.Green).MCvScalar, 1);
               }
               else
               {
                   CvInvoke.Line(image, line.P1, line.P2, new Bgr(Color.Red).MCvScalar, 1);
               }

               countFlag = !countFlag;
           }
       }

       public static void RoofRectangles(UMat image, Roof[] roofs)
       {
           bool countFlag = true;
           Random rand = new Random();

           foreach (var roof in roofs)
           {
               Draw.Rectangle(image, roof.Rectangle, countFlag ? Color.Pink : Color.GreenYellow, rand.Next(1, 10));
               countFlag = !countFlag;
           }
       }

        public static void Houses(UMat image, House[] houses)
        {
            foreach (var house in houses)
            {
                Draw.Rectangle(image, house.WallsRectangle, Color.White, 2);

                Draw.Lines(
                    image,
                    new[]
                    {house.Roof.Line1, house.Roof.Line2}
                    );
            }
        }

    }
}
