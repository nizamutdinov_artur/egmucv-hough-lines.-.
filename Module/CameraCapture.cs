﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.CvEnum;

namespace ShapeDetection.Module
{
   public class CameraCapture
   {
       private MainForm _mainForm;
       private Capture _capture;
       private Timer _timer;

      public CameraCapture(MainForm mainForm)
      {
          _mainForm = mainForm;

           _capture = new Capture();


          _timer = new Timer();
          _timer.Enabled = true;
          _timer.Tick += OnTimerTick;
      }

      private void OnTimerTick(object sender, EventArgs eventArgs)
       {

       }
    }
}
