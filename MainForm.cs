using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System.Diagnostics;
using ShapeDetection.Container;
using ShapeDetection.Module;

namespace ShapeDetection
{
    public partial class MainForm : Form
    {
        private Capture cameraCapture;

        public MainForm()
        {
            InitializeComponent();

            CvInvoke.UseOpenCL = false;

            fileNameTextBox.Text = "image.jpg";

        }

        /// <summary>
        /// Gets the image from file and resize it.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>The image.</returns>
        private Image<Bgr, Byte> GetImage(string path)
        {
            Image<Bgr, byte> image = new Image<Bgr, byte>(fileNameTextBox.Text);
            return image;
        }

        /// <summary>
        /// Removes the noise by gaussian pyramid decomposition.
        /// </summary>
        /// <param name="uimage">The uimage.</param>
        /// <returns>Filtered uimage.</returns>
        private UMat RemoveNoiseByPyr(UMat uimage)
        {
            UMat pyrDown = new UMat();
            CvInvoke.PyrDown(uimage, pyrDown);

            UMat pyrUp = new UMat();
            CvInvoke.PyrUp(pyrDown, pyrUp);

            return pyrUp;
        }

        /// <summary>
        /// Calls a Canny egde alghoritm.
        /// </summary>
        /// <param name="uimage">The uimage.</param>
        /// <returns>The Canny edges uimage.</returns>
        private UMat Canny(UMat uimage, double treshold, double tresholdLinking)
        {
            double cannyThreshold = treshold;
            double cannyThresholdLinking = tresholdLinking;

            UMat cannyEdges = new UMat();
            CvInvoke.Canny(uimage, cannyEdges, cannyThreshold, cannyThresholdLinking);

            return cannyEdges;
        }

        private UMat DrawHousesImage(LineSegment2D[] lines, Size imageSize)
        {
            UMat image = new UMat(imageSize, DepthType.Cv8U, 3);
            image.SetTo(new MCvScalar(0));

           // Draw.LineRectangles(lineImage, lines);
           // Draw.Lines(lineImage, lines);

           // Roof[] roofs = Recognition.FindRoofs(lines);
           // Draw.RoofRectangles(image, roofs);

            House[] houses = Recognition.FindHouses(lines);
            Draw.Houses(image, houses);

            return image;
        }

        private UMat DrawLineImage(LineSegment2D[] lines, Size imageSize)
        {
            UMat lineImage = new UMat(imageSize, DepthType.Cv8U, 3);
            lineImage.SetTo(new MCvScalar(0));

            Draw.LineRectangles(lineImage, lines);
            Draw.Lines(lineImage, lines);

            return lineImage;
        }

        public void PerformShapeDetection(Image<Bgr, Byte> img)
        {
            if (fileNameTextBox.Text != String.Empty)
            {
                img.Resize(1200, 1000, Emgu.CV.CvEnum.Inter.Cubic, false);
                StringBuilder msgBuilder = new StringBuilder("Performance: ");


                UMat uimage = img.ToUMat();   

                //Convert the image to grayscale and filter out the noise  
                //CvInvoke.CvtColor(img, uimage, ColorConversion.Bgr2Gray);

                //use image pyr to remove noise
                uimage = RemoveNoiseByPyr(uimage);
                uimage = RemoveNoiseByPyr(uimage);
                uimage = RemoveNoiseByPyr(uimage);
                uimage = RemoveNoiseByPyr(uimage);

                #region Canny and edge detection   

                Stopwatch watch = Stopwatch.StartNew();

                UMat cannyEdges = Canny(uimage, 180, 120);

                LineSegment2D[] lines = CvInvoke.HoughLinesP(
                    cannyEdges,
                    1, //Distance resolution in pixel-related units
                    Math.PI/360.0, //Angle resolution measured in radians.
                    30, //threshold
                    30, //min Line width
                    10); //gap between lines


                watch.Stop();
                msgBuilder.Append(String.Format("Detect lines - {0} ms; ", watch.ElapsedMilliseconds));

                #endregion

                #region CombineLinkedLines

                watch.Reset();
                watch.Start();

                lines = LineOperation.CombineLinkedLines(lines);

                watch.Stop();
                msgBuilder.Append(String.Format("Link lines - {0} ms; ", watch.ElapsedMilliseconds));

                #endregion

                #region draw

                originalImageBox.Image = uimage;

                secondImageBox.Image = DrawLineImage(lines, img.Size);


                watch.Reset();
                watch.Start();

                thirdImageBox.Image = DrawHousesImage(lines, img.Size);

                watch.Stop();
                msgBuilder.Append(String.Format("House recornize - {0} ms; ", watch.ElapsedMilliseconds));


                

                #endregion

                Text = msgBuilder.ToString();

            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            PerformShapeDetection(GetImage(fileNameTextBox.Text));
        }

        private void loadImageButton_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK || result == DialogResult.Yes)
            {
                fileNameTextBox.Text = openFileDialog1.FileName;

                PerformShapeDetection(GetImage(fileNameTextBox.Text));
            }
        }

        private void bufferButton_Click(object sender, EventArgs e)
        {
            if (Clipboard.ContainsImage())
            {
                Bitmap masterImage = (Bitmap)Clipboard.GetImage();
                Image<Bgr, Byte> image = new Image<Bgr, Byte>(masterImage);

                PerformShapeDetection(image);
            
            }
        }

        private void webCamBox_CheckedChanged(object sender, EventArgs e)
        {
            if (cameraCapture != null)
            {
                if (!webCamBox.Checked)
                {
                    //stop the capture
                    webCamBox.Text = "Start Capture";
                    cameraCapture.Stop();
                }
                else
                {
                    //start the capture
                    webCamBox.Text = "Stop";
                    cameraCapture.Start();
                }
            }
            else
            {
                cameraCapture = new Capture();
                cameraCapture.FlipHorizontal = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (webCamBox.Checked)
            {
                Mat frame = new Mat();
                cameraCapture.Retrieve(frame, 0);

                PerformShapeDetection(frame.ToImage<Bgr, Byte>());
            }
        }

    }
}
