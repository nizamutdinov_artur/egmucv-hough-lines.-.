﻿using System.Drawing;
using Emgu.CV.Structure;

namespace ShapeDetection.Container
{

    /// <summary>
    /// The roof container class.
    /// </summary>
    public class Roof
    {
        #region Fields

        private Rectangle _rectangle;
        private LineSegment2D _line1;
        private LineSegment2D _line2;

        #endregion

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="Roof"/> class.
        /// </summary>
        /// <param name="rectangle">The rectangle.</param>
        /// <param name="line1">The line1.</param>
        /// <param name="line2">The line2.</param>
        public Roof(Rectangle rectangle, LineSegment2D line1, LineSegment2D line2)
        {
            _rectangle = rectangle;
            _line1 = line1;
            _line2 = line2;
        }

        #endregion

        #region Props

        /// <summary>
        /// Gets or sets the rectangle.
        /// </summary>
        /// <value>
        /// The rectangle.
        /// </value>
        public Rectangle Rectangle
        {
            get { return _rectangle; }
            set { _rectangle = value; }
        }

        /// <summary>
        /// Gets or sets the line1.
        /// </summary>
        /// <value>
        /// The line1.
        /// </value>
        public LineSegment2D Line1
        {
            get { return _line1; }
            set { _line1 = value; }
        }

        /// <summary>
        /// Gets or sets the line2.
        /// </summary>
        /// <value>
        /// The line2.
        /// </value>
        public LineSegment2D Line2
        {
            get { return _line2; }
            set { _line2 = value; }
        }

        #endregion
    }
}
