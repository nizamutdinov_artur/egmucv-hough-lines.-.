﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShapeDetection.Module
{
   public class MathHelper
    {
       public static bool ValueInRange(int value, int first, int last)
       {
           if (first > last)
               return value <= first && value >= last;

           return value >= first && value <= last;
       }
    }
}
