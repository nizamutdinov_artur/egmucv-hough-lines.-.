﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Emgu.CV.Structure;

namespace ShapeDetection.Module
{

    /// <summary>
    /// Class thats support operations with lines.
    /// </summary>
   public class LineOperation
    {
       
        /// <summary>
        /// Angle beetween line and abscissa.
        /// </summary>
        /// <param name="line">The line.</param>
        /// <returns>The angle.</returns>
       public static double AngleWithAbscissa(LineSegment2D line)
       {
           var abscissa = new LineSegment2D(
               new Point(0, 0),
               new Point(0, 1));

           return abscissa.GetExteriorAngleDegree(line);
       }

       /// <summary>
       /// Get the line rectangle.
       /// </summary>
       /// <param name="line">The line.</param>
       /// <returns>The Rectangle.</returns>
       public static Rectangle LineRectangle(LineSegment2D line)
       {
           var rectangle = new Rectangle
           {
               Y = line.P1.Y < line.P2.Y ? line.P1.Y : line.P2.Y, 
               X = line.P1.X < line.P2.X ? line.P1.X : line.P2.X, 
               Width = Math.Abs(line.P1.X - line.P2.X), 
               Height = Math.Abs(line.P1.Y - line.P2.Y)
           };

           return rectangle;
       }

       /// <summary>
       /// Determines whether line1 can be combined with line2.
       /// </summary>
       /// <param name="line1">The line1.</param>
       /// <param name="line2">The line2.</param>
       /// <returns>Bool.</returns>
       private static bool CanBeCombined(LineSegment2D line1, LineSegment2D line2)
       {
           Rectangle line1Rectangle = LineRectangle(line1);
           Rectangle line2Rectangle = LineRectangle(line2);

           const int increaseSizePx = 10;

           line1Rectangle.X -= increaseSizePx/2;
           line1Rectangle.Y -= increaseSizePx/2;
           line1Rectangle.Width += increaseSizePx;
           line1Rectangle.Height += increaseSizePx;

           line2Rectangle.X -= increaseSizePx / 2;
           line2Rectangle.Y -= increaseSizePx / 2;
           line2Rectangle.Width += increaseSizePx;
           line2Rectangle.Height += increaseSizePx;

           if (line1Rectangle.IntersectsWith(line2Rectangle))
           {
               if (line1.GetExteriorAngleDegree(line2) < 10 && line1.GetExteriorAngleDegree(line2) > -10)
               {
                       return true;
               }
           }

           return false;
       }

       /// <summary>
       /// Is line increase ?
       /// </summary>
       /// <param name="line">The line.</param>
       /// <returns>Bool.</returns>
       public static bool LineIncreases(LineSegment2D line)
       {
           if (line.P1.X > line.P2.X)
           {
               var buffer = line.P1;
               line.P1 = line.P2;
               line.P2 = buffer;
           }

           return line.P1.Y < line.P2.Y;
       }

       /// <summary>
       /// Combines the two lines.
       /// </summary>
       /// <param name="line1">The line1.</param>
       /// <param name="line2">The line2.</param>
       /// <returns></returns>
       private static LineSegment2D CombineTwoLines(LineSegment2D line1, LineSegment2D line2)
       {
           Point p1 = new Point();
           Point p2 = new Point();

           p1.X = line1.P1.X < line2.P1.X ? line1.P1.X : line2.P1.X;
           p2.X = line1.P2.X > line2.P2.X ? line1.P2.X : line2.P2.X;

           if (LineIncreases(line1))
           {
               p1.Y = line1.P1.Y < line2.P1.Y ? line1.P1.Y : line2.P1.Y;
               p2.Y = line1.P2.Y > line2.P2.Y ? line1.P2.Y : line2.P2.Y;
           }
           else
           {
               p1.Y = line1.P1.Y > line2.P1.Y ? line1.P1.Y : line2.P1.Y;
               p2.Y = line1.P2.Y < line2.P2.Y ? line1.P2.Y : line2.P2.Y;
           }

           return new LineSegment2D(p1, p2);
       }

       /// <summary>
       /// Combines the linked lines.
       /// </summary>
       /// <param name="inputLines">The input lines.</param>
       /// <returns></returns>
       public static LineSegment2D[] CombineLinkedLines(LineSegment2D[] inputLines)
       {
           List<LineSegment2D> lines = inputLines.ToList();

           for (int i = 0; i < lines.Count(); i++)
           {
               if (lines.Count() <= i)
                   break;

               for (int j = 0; j < lines.Count(); j++)
               {
                   if (lines.Count <= i)
                       break;

                   if (lines[i].Equals(lines[j]))
                       continue;


                   if (CanBeCombined(lines[i], lines[j]))
                   {
                       lines[i] = CombineTwoLines(lines[i], lines[j]);
                       lines.RemoveAt(j);
                   }
               }
           }

           return lines.ToArray();

       }

    }
}
